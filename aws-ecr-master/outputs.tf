output "arn" {
  description = "Full ARN of the repository."
  value       = "${aws_ecr_repository.default.arn}"
}

output "url" {
  description = "The URL of the repository (in the form aws_account_id.dkr.ecr.region.amazonaws.com/repositoryName )"
  value       = "${aws_ecr_repository.default.repository_url}"
}