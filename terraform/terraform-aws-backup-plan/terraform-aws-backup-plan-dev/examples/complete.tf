module "backup_plan-NAME" {
  source = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-backup-plan.git?ref=master"
  name   = "NAME"

  rules = [
    {
      rule_name         = "Diario"
      target_vault_name = "NAME"
      schedule          = "cron(0 5 ? * * *)"

      lifecycle = [
        {
          delete_after = "7"
        },
      ]
    },
  ]
}
