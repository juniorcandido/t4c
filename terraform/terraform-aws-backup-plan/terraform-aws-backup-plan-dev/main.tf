locals {
  iam_policies = [
    "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup",
    "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForRestores",
  ]
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["backup.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "default" {
  name               = "${var.name}"
  assume_role_policy = "${data.aws_iam_policy_document.assume_role_policy.json}"
  path               = "/service-role/"
  description        = "${var.description}"
}

resource "aws_iam_role_policy_attachment" "default" {
  count      = "${length(local.iam_policies) > 0 ? length(local.iam_policies) : 0}"
  role       = "${aws_iam_role.default.name}"
  policy_arn = "${local.iam_policies[count.index]}"
}

resource "aws_backup_vault" "default" {
  name = "${var.name}"
}

resource "aws_backup_plan" "default" {
  name       = "${var.name}"
  rule       = ["${var.rules}"]
  depends_on = ["aws_backup_vault.default"]
}
