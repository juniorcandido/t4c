variable "name" {
  description = "Name of backup plan"
  type        = "string"
}

variable "description" {
  description = "The description of the backup plan"
  default     = "Managed by Terraform"
  type        = "string"
}

variable "rules" {
  description = "List of rules of backup plan"
  default     = []
}
