module "backup_plan_resource-NAME" {
  source         = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-backup-resource.git?ref=master"
  name           = "NAME"
  backup_plan_id = "${module.backup_plan-NAME.plan_id}"
  iam_role_arn   = "${module.backup_plan-NAME.role_arn}"

  resources = [
    "arn:aws:ec2:us-east-1:ACCOUNT_ID:volume/vol-xxxxxx",
  ]
}
