resource "aws_cloudfront_distribution" "default" {
  enabled             = "${var.enabled}"
  is_ipv6_enabled     = "${var.is_ipv6_enabled}"
  comment             = "${var.comment}"
  default_root_object = "${var.default_root_object}"
  aliases             = ["${var.aliases}"]
  price_class         = "${var.price_class}"

  origin {
    domain_name = "${var.origin_domain_name}"
    origin_id   = "${var.distribution_name}"
    origin_path = "${var.origin_path}"

    custom_origin_config {
      http_port                = "${var.origin_http_port}"
      https_port               = "${var.origin_https_port}"
      origin_protocol_policy   = "${var.origin_protocol_policy}"
      origin_ssl_protocols     = "${var.origin_ssl_protocols}"
      origin_keepalive_timeout = "${var.origin_keepalive_timeout}"
      origin_read_timeout      = "${var.origin_read_timeout}"
    }
  }

  default_cache_behavior {
    allowed_methods  = "${var.allowed_methods}"
    cached_methods   = "${var.cached_methods}"
    target_origin_id = "${var.distribution_name}"
    compress         = "${var.compress}"

    forwarded_values {
      headers = ["${var.forward_headers}"]

      query_string = "${var.forward_query_string}"

      cookies {
        forward           = "${var.forward_cookies}"
        whitelisted_names = ["${var.forward_cookies_whitelisted_names}"]
      }
    }

    viewer_protocol_policy = "${var.viewer_protocol_policy}"
    default_ttl            = "${var.default_ttl}"
    min_ttl                = "${var.min_ttl}"
    max_ttl                = "${var.max_ttl}"
  }

  custom_error_response = ["${var.custom_error_response}"]

  viewer_certificate {
    acm_certificate_arn      = "${var.acm_certificate_arn}"
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.1_2016"
  }

  restrictions {
    geo_restriction {
      restriction_type = "${var.geo_restriction_type}"
      locations        = "${var.geo_restriction_locations}"
    }
  }
}

data "aws_route53_zone" "default" {
  count   = "${var.route53_enabled == "true" ? signum(length(compact(var.aliases))) : 0}"
  zone_id = "${var.route53_parent_zone_id}"
  name    = "${var.route53_parent_zone_name}"
}

resource "aws_route53_record" "default" {
  count   = "${var.route53_enabled == "true" ? length(compact(var.aliases)) : 0}"
  zone_id = "${data.aws_route53_zone.default.zone_id}"
  name    = "${element(compact(var.aliases), count.index)}"
  type    = "A"

  alias {
    name                   = "${aws_cloudfront_distribution.default.domain_name}"
    zone_id                = "${aws_cloudfront_distribution.default.hosted_zone_id}"
    evaluate_target_health = "${var.route53_evaluate_target_health}"
  }
}
