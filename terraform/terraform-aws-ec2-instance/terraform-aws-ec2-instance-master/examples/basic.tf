module "ec2_instance-vpn" {
  source                  = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-ec2-instance.git?ref=master"
  name                    = "vpn"
  disable_api_termination = false
  ami                     = "ami-xxxx"
  instance_type           = "t2.micro"
  key_name                = "vpn"
  vpc_security_group_ids  = ["${module.sg-xxx-vpn.this_security_group_id}"]
  subnet_ids              = "${module.vpc.public_subnets}"

  tags = {
    Terraform = "true"
  }
}
