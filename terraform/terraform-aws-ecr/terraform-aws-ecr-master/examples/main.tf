module "ecr-NAME" {
  source              = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-ecr.git?ref=master"
  name                = "NAME"
  max_images_retained = "5"

  role_arn = [
    "arn:aws:iam::xxxxx:role/xxxxxx",
  ]
}
