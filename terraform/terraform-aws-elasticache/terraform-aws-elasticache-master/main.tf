locals {
  security_group_name = "${var.name}-elasticache-${var.engine}"
}

resource "aws_elasticache_cluster" "redis" {
  count                    = "${var.engine == "redis" ? 1 : 0}"
  cluster_id               = "${var.name}"
  engine                   = "${var.engine}"
  parameter_group_name     = "${aws_elasticache_parameter_group.default.name}"
  subnet_group_name        = "${aws_elasticache_subnet_group.default.name}"
  security_group_ids       = ["${aws_security_group.default.id}"]
  num_cache_nodes          = "${var.num_cache_nodes}"
  node_type                = "${var.node_type}"
  engine_version           = "${var.engine_version}"
  port                     = "${var.port}"
  maintenance_window       = "${var.maintenance_window}"
  snapshot_window          = "${var.snapshot_window}"
  snapshot_retention_limit = "${var.snapshot_retention_limit}"
  apply_immediately        = "${var.apply_immediately}"
  tags                     = "${merge(map("Name", var.name), var.tags)}"
}

resource "aws_elasticache_cluster" "memcached" {
  count                        = "${var.engine == "memcached" ? 1 : 0}"
  cluster_id                   = "${var.name}"
  engine                       = "${var.engine}"
  parameter_group_name         = "${aws_elasticache_parameter_group.default.name}"
  subnet_group_name            = "${aws_elasticache_subnet_group.default.name}"
  security_group_ids           = ["${aws_security_group.default.id}"]
  num_cache_nodes              = "${var.num_cache_nodes}"
  node_type                    = "${var.node_type}"
  engine_version               = "${var.engine_version}"
  port                         = "${var.port}"
  preferred_availability_zones = "${var.preferred_availability_zones}"
  az_mode                      = "${var.az_mode}"
  maintenance_window           = "${var.maintenance_window}"
  apply_immediately            = "${var.apply_immediately}"
  tags                         = "${merge(map("Name", var.name), var.tags)}"
}

resource "aws_elasticache_parameter_group" "default" {
  name        = "${var.name}"
  family      = "${var.family}"
  description = "${var.description}"
  parameter   = "${var.parameter}"
}

resource "aws_elasticache_subnet_group" "default" {
  name        = "${var.name}"
  subnet_ids  = ["${var.subnet_ids}"]
  description = "${var.description}"
}

resource "aws_security_group" "default" {
  name   = "${local.security_group_name}"
  vpc_id = "${var.vpc_id}"
  tags   = "${merge(map("Name", local.security_group_name), var.tags)}"
}

resource "aws_security_group_rule" "ingress" {
  count                    = "${length(var.source_security_group_id) > 0 ? length(var.source_security_group_id) : 0}"
  type                     = "ingress"
  from_port                = "${var.port}"
  to_port                  = "${var.port}"
  protocol                 = "tcp"
  source_security_group_id = "${element(var.source_security_group_id, count.index)}"
  security_group_id        = "${aws_security_group.default.id}"
  description              = "${var.description}"
}

resource "aws_security_group_rule" "egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.default.id}"
  description       = "${var.description}"
}
